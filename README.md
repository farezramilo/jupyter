# Required
- Docker

# Setup
- Run `docker pull adiog/cling-jupyter`
- Run `docker-compose up -d`
- Open in browser `http://127.0.0.1:8888/`

### Files location
`./exams`